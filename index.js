

document.addEventListener('DOMContentLoaded', function(event) {
    /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
    // When the user scrolls the page, execute myFunction


    // Get the header

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position

    // When the user scrolls the page, execute myFunction
        window.onscroll = function() {myScrollFunction()};

        // Get the header
        var header = document.getElementById("mainPageHeader");

        // Get the offset position of the navbar
        var sticky = header.offsetTop;

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myScrollFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }        
  });
    /* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */
    function showHamburgerFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
    x.style.display = "none";
    } else {
    x.style.display = "block";
    }
}

